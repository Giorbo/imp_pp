use "stack.sml";


(* ritorna una locazione inutilizzata o nuova *)
fun getNewLocation (S:Store, nil) = (DynamicArray.bound S + 1, nil)
    | getNewLocation (S:Store, head::tail) = (head, tail)
fun getLocation (Env: Environment, v: varname): Loc = top (HashTable.lookup Env v)
fun getValue (S, l) = DynamicArray.sub (S, l)

(* Valuta un Lval, ritornandone la locazione *)
fun LvalToLoc(Env, Variable(x), S):Loc = 
    getLocation(Env, x)
  | LvalToLoc(Env, Deref(x), S):Loc =
    let val l = LvalToLoc(Env, x, S)
    val Ptr(l') = getValue(S, l)
    in l'
    end

(* Ritorna il valore dell'espressione nell'ambiente Env e nello store S *)
fun EvalExp (Env, Val k, S) = k
  | EvalExp (Env, LvalExp x, S) =
    let val l = LvalToLoc(Env, x, S)
    in getValue(S, l)
    end
  | EvalExp (Env, Plus (M, N), S) =
    let val (Int v1) = EvalExp (Env, M, S)
        val (Int v2) = EvalExp (Env, N, S)
    in Int(v1 + v2)
    end

  | EvalExp (Env, LessThan(M, N), S) =
    let val (Int v1) = EvalExp (Env, M, S)
        val (Int v2) = EvalExp (Env, N, S)
    in Bool(v1 < v2)
    end
  | EvalExp (Env, Ref(x), S) = 
    Ptr(LvalToLoc(Env, x, S))

(* Semplice toString di una Const, per i puntatori ritorna 
  "****s" con un '*' per ogni livello di puntatori e con s che è 
  = strOfConst del puntato
*)
fun strOfConst (Int x) S = Int.toString (x)
    | strOfConst (Bool x) S = Bool.toString (x)
    | strOfConst (Ptr x) S = 
        "*"^strOfConst (getValue(S, x)) S

