use "imp.sml";

val env: Environment = HashTable.mkTable(HashString.hashString, op=)(42, Fail "not found");
val store: Store = DynamicArray.array(10, Int 0);
val refCount: Rc = DynamicArray.array(10, 0);


fun evex exp = EvalExp(env, exp, store)




(*"zucchero sintattico": concatena una lista di istruzioni usando Then*)
fun block (head::nil) = head 
    | block(head :: tail) = Then(head, block(tail))
    | block(nil) = Skip


(*Non è un linguaggio tipato!*)
(*
val v = 2+3 in
    Print(v);
    v = true;
    Print(v);
*)
val p = evp(
    Declare("v", Plus(Val(Int 2), Val(Int 3)), 
    block [
        Print(LvalExp(Variable "v")),
        Assign(Variable("v"), Val(Bool true)),
        Print(LvalExp(Variable "v"))
    ])
)


(*sum from 1 to n*)
(*
let val sum = 0
in let val i = 0
   in while i < 10 do
        i := 1 + i;
        sum := sum + i;
        print sum
*)

val n = 10;
val p2 =
    evp(Declare("sum", Val(Int 0),
            Declare("i", Val(Int 0),
                While(LessThan(LvalExp(Variable("i")), Val(Int n)),
                    block [
                    Assign(Variable("i"), Plus(Val(Int 1), LvalExp(Variable("i")))),
                    Assign(Variable("sum"), Plus(LvalExp(Variable("sum")), LvalExp(Variable("i")))),
                    Print (LvalExp(Variable("sum")))
                    ]
                )
            )
       )
    )

(*
let x = 42 in
let y = 0 in
let p = &x in
    print( *p);
    p = &y;
    print( *p)
*)

val p = Declare("x", Val(Int 42),
        Declare("y", Val(Bool false),
        Declare("p", Ref(Variable "x"),
        block[
            Print(LvalExp(Deref(Variable "p"))),
            Assign(Variable("p"), Ref(Variable "y")),
            Print(LvalExp(Deref(Variable "p")))
        ]
        )
        )
    )
    

val _ = evp(p);


(* programma per testare l'utilità dei puntatori:
un puntatore può puntare una variabile anche da fuori il suo scope. 
Infatti la variabile viene eliminata dall'environment ma la locazione non viene
pulita se puntata.
*)
(*
val pointer = 0 in
    val pointed = 10 in
        pointer := &pointed
    Print ( *pointer )
*)
val p = Declare("pointer", Val(Int 0), 
        block [
            Declare("pointed", Val(Int 10),
            Assign(Variable("pointer"), Ref(Variable "pointed"))
            ),
            Print(LvalExp(Deref(Variable ("pointer"))))
        ]
        )

val t = evp(p);



val n = 10;

(*questo programma è fatto per testare la garbage collection, 
infatti local viene reistanziata ogni ciclo, ma sempre nella stessa locazione*)
(*
val i = 0;
while i < n {
    int local = i;
    Print(i);
    i = i+1
}*)
val p2 =
    evp(Declare("i", Val(Int 0),
            While(LessThan(LvalExp(Variable("i")), Val(Int n)),
                Declare("local", LvalExp(Variable("i")), 
                    block [Print(LvalExp(Variable "i") ), 
                    Assign(Variable "i", Plus(Val(Int 1), LvalExp(Variable("i"))))
                    ])
            )
        )
    )


(*

a*)
val p = evp(
    Declare("a", Val(Int 0), 
        Declare("p1", Ref(Variable "a"),
        Declare("p2", Ref(Variable "p1"),
        Declare("p2_2", Ref(Deref(Deref(Variable "p2"))),
            block[Print(LvalExp(Variable "p2_2")), Print(LvalExp(Variable "p2"))]
        )))
    )
)