use "expr.sml";

(* aggiunge una variabile x con locazione l all'environment *)
fun addVar (env: Environment, x: string, l: Loc) =
    if HashTable.inDomain env x then
        let val locStack = HashTable.lookup env x
        in HashTable.insert env (x, Push (l, locStack))
        end
    else
        HashTable.insert env (x, Push(l, Empty))

(* rimuove una variabile x dall'environment *)
fun dropVar (env:Environment, x:varname) =
    let val Push (oldLoc, remStack) = HashTable.lookup env x
    in HashTable.insert env (x, remStack)
    end

(* decrementa il refCount di l e la aggiunge eventualmente a canuse *)
fun decrementRc (refCount, l, canuse) = 
    (DynamicArray.update(refCount, l, getValue(refCount, l)-1);
    if getValue(refCount, l) = 0 then
      l::canuse
    else canuse)


(* modifica il valore di una locazione e aggiorna refCount e canuse *)
fun setLoc (store: Store, l:Loc, v: Const, refCount, canuse) =
    (* se Store[l] era puntatore refcount[Store[l]]-- *)
    let val old = getValue(store, l)
    val _ = DynamicArray.update(store, l, v)
    val _ = case v of
      Ptr l1 => DynamicArray.update(refCount, l1, getValue(refCount, l1)+1) 
      | _ => ()
    val canuse' = case old of
      Ptr l1 => decrementRc(refCount, l1, canuse)
      | _ => canuse
    in canuse'
    end

fun println (x:string) = print(x^"\n")

(* Valuta un programma p. Conviene chiamare questa funzione tramite 
evp*)
fun EvalProg (Env, Skip, S, refCount, canuse) = (S, canuse)
  | EvalProg (Env, Then (Prog1, Prog2), S, refCount, canuse) =
    let val (S', canuse) = EvalProg (Env, Prog1, S, refCount, canuse)
    in EvalProg (Env, Prog2, S', refCount, canuse)
    end

  | EvalProg (Env, If (M, Prog1, Prog2), S, refCount, canuse) =
    let val (Bool b) = EvalExp (Env, M, S)
    in if b
       then EvalProg (Env, Prog1, S, refCount, canuse)
       else EvalProg (Env, Prog2, S, refCount, canuse)
    end

  | EvalProg (Env, While (Exp, Prog), S, refCount, canuse) =
    let val (Bool b) = EvalExp (Env, Exp, S)
    in if b
       then
           let val (S', canuse) = EvalProg (Env, Prog, S, refCount, canuse)
           in EvalProg (Env, While (Exp, Prog), S', refCount, canuse)
           end
       else
           (S, canuse)
    end
  | EvalProg (Env, Declare (Str, Exp, Prog), S, refCount, canuse) =
    let val v = EvalExp (Env, Exp, S)
        val (l,canuse) = getNewLocation (S, canuse)
        val _ = DynamicArray.update(refCount, l, 1)
        val _ = addVar (Env, Str, l)
        val canuse = setLoc (S, l, v, refCount, canuse)
        val (S', canuse) = EvalProg (Env, Prog, S, refCount, canuse)
        val _ = dropVar(Env, Str);
        val canuse = decrementRc (refCount, l, canuse)
        val canuse = case getValue(S, l) of
          Ptr l1 => decrementRc (refCount, l1, canuse)
          | _ => canuse
        (* refcount[l]--*)
    in (S', canuse)
    end
  | EvalProg (Env, Assign (L, Exp), S, refCount, canuse) =
    let val v = EvalExp (Env, Exp, S)
        val l = LvalToLoc(Env, L, S)
        val canuse = setLoc (S, l, v, refCount, canuse);
    in (S, canuse)
    end
  | EvalProg (Env, Print(Exp), S, refCount, canuse) =
    let val v = EvalExp(Env, Exp, S);
    in
       println(strOfConst v S);
       (S, canuse)
    end


(* valuta prog tramite EvalProg, fornendo 
ambiente, store, refCount e canuse vuoti alla funzione
*)
fun evp prog =
    (println("evaluating:");
     EvalProg(HashTable.mkTable(HashString.hashString, op=)(42, Fail "not found"), 
            prog, DynamicArray.array(10, Int 0), DynamicArray.array(10, 0), []);
     print("\n\n");true)
