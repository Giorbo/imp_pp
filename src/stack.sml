use "types.sml";

datatype Stack = Empty
               | Push of Loc * Stack

exception EmptyStackException;

fun top (Push(tp, rest)): Loc = tp
    | top (Empty) = raise EmptyStackException

fun pop (Push(tp, rest)): Stack = rest
    | pop (Empty) = raise EmptyStackException


(* Env: Var * Stack(Loc) *)
type Environment = (string, Stack) HashTable.hash_table

