type Loc = int

type varname = string

datatype Const = Int of int
               | Bool of bool
               | Ptr of Loc


type Store = (Const) DynamicArray.array
type Rc = (int) DynamicArray.array  


datatype Lval = Variable of varname
              | Deref of Lval (* "*" *)

datatype Expression = Val of Const
                    | LvalExp of Lval
                    | Plus of Expression * Expression
                    | LessThan of Expression * Expression
                    | Ref of Lval (* "&" *)

datatype Program = Skip
                 | Then of Program * Program
                 | If of Expression * Program * Program
                 | While of Expression * Program
                 | Declare of varname * Expression * Program
                 | Assign of Lval * Expression
                 | Print of Expression
